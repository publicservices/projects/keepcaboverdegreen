/* <link rel="preconnect" href="https://fonts.gstatic.com">
	 <link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@300&display=swap" rel="stylesheet"> */

class FontsLoaded extends HTMLElement {
	connectedCallback() {
		this.fontUrl = this.getAttribute('font-url')
		this.fontPreloadUrl = this.getAttribute('font-preload-url')
		this.targetElement = this.getAttribute('target-element') || 'html'
		this.targetClass = this.getAttribute('target-class')

		if (!this.fontUrl) {
			console.info('No font-url to load.')
			return
		}

		this.$targetElement = document.querySelector(this.targetElement)
		if (!this.$targetElement) {
			console.log('Could not find target-element', this.targetElement)
			return
		}

		if (this.targetClass) {
			this.$targetElement.classList.remove(this.targetClass)
		} else {
			this.$targetElement.setAttribute('font-loaded', false)
		}

		this.loadFonts()
	}

	loadFonts = async () => {
		/* if (this.fontPreloadUrl) {
			 const $fontPreloadLink = document.createElement('link')
			 $fontPreloadLink.setAttribute('rel', 'preload')
			 $fontPreloadLink.setAttribute('href', this.fontLoadedUrl)
			 $fontPreloadLink.setAttribute('as', 'style')
			 document.querySelector('head').append($fontPreloadLink)
			 } */

		try {
			await new Promise(resolve => {
				const $fontLink = document.createElement('link')
				$fontLink.setAttribute('href', this.fontUrl)
				$fontLink.setAttribute('rel', 'stylesheet')
				$fontLink.onload = () => {
					resolve()
				}
				document.querySelector('head').append($fontLink)
			})
		} catch(error) {
			console.log('Could not load font from url', $fontLink, error)
			return
		}

		if (this.targetClass) {
			this.$targetElement.classList.add(this.targetClass)
		} else {
			this.$targetElement.setAttribute('font-loaded', true)
		}
	}
}

export default FontsLoaded
