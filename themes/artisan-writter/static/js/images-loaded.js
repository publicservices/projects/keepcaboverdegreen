const template = document.createElement('template')

class ImagesLoaded extends HTMLElement {
	connectedCallback() {
		this.imgs = document.querySelectorAll('img')
		Object.entries(this.imgs).forEach(img => {
			let i = img[1]
			if (i.complete || i.naturalHeight !== 0) {
				i.setAttribute('loaded', true)
			} else {
				i.addEventListener('load', this.handleLoaded)
			}
		})
	}
	handleLoaded = event => {
		event.target.setAttribute('loaded', true)
		event.target.removeEventListener('load', this.handleLoaded)
	}
}

export default ImagesLoaded
