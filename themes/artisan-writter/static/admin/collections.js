import articles from './model-articles.js'
import pages from './model-pages.js'
import series from './model-series.js'
import themes from './model-themes.js'
import configs from './model-configs.js'

export default [
    articles,
    series,
    themes,
    pages,
    configs
]
