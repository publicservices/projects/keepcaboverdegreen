export default {
    label: 'Featured Image Caption',
    name: 'image_caption',
    widget: 'markdown',
    required: false,
    hint: 'The caption/credits for the featured image'
}
