const documentReady = () => {
    if (document.readyState === 'complete') {
	initWebsite()
    }
}

const initWebsite = () => {
    console.log('Keep Cabo Verde Green!')
}

document.onreadystatechange = documentReady
