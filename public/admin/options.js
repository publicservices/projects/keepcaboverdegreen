import collections from './collections.js'

export default {
    config: {
	// Skips config.yml.
	// By not skipping, the configs will be merged, with the js-version taking priority.
	load_config_file: false,
	display_url: window.location.origin,

	backend: {
	    name: 'gitlab',
	    branch: 'main',
	    repo: 'publicservices/keepcaboverdegreen',
	    auth_type: 'implicit',
	    app_id: '5d45b6c07abe31806f91e814cebb694557cba53c903d93d8207cb3d2ef16826d'
	},

	media_folder: 'static/media/uploads',
	public_folder: '/media/uploads',

	collections
    }
}
