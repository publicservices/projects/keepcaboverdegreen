+++
title = "Le café au Cap-Vert"
serie = "cabo-verde"
slug = "le-cafe-au-cap-vert"
draft = false
language = "fr"
themes = ["travel"]
date_published = "2019-05-09"
image = "/media/uploads/dscf0825.jpg"
image_caption = "Café sur l'île de Santo Antão"

[[links]]
link = "*Video [Djar Fogo Coffee Spirit](https://youtu.be/-FYTF9_KOC0) en portugais sous titré en anglais.*"
+++
Je ne peux commencer une bonne journée sans un bon café. Et vous ? 

Pour mon blog, c'est pareil alors mon premier sujet sera consacré au café du Cap-Vert. Introduit par les portugais à la fin du 18ème siècle dans trois îles essentiellement (São Nicolau, Santo Antão et Fogo), le Cap-Vert c'est 300 ans d'histoire du Café.

## À Santo Antão, le café familiale

Sur l'île de Santo Antão, au village de Lombo Branco, j'ai dégusté le meilleur café de ma vie! Que de moments inoubliables chez mon oncle Izé et ma tante Antonia, entre autre autour d'un café au petit-déjeuner. 

![Tia Antonia](/media/uploads/img_7446.jpg)

![Tio Izé Louro](/media/uploads/dscf0739.jpg)

Pour arriver jusque leur petite terre de café, l'accès n’est pas facile dans la montagne de roche blanche. À des centaines de mètres d’altitude, les pieds de café apparaissent à l'ombre d'arbres fruitiers. 

Les facteurs géographiques et climatiques important dans la culture du café sont: une haute altitude, un peu de pluie, un peu de soleil, des vents venant de l'océan et de la fraîcheur.

![Du café la haut sur cette montagne (Lombo Branco)](/media/uploads/dscf0793.jpg)

## A Fogo, le café écoresponsable D'jar Fogo

Après Santo Antão, nous changeons d'île. L'île de Fogo a un microclimat unique. Cette île volcanique dispose  d'un sol fertile particulièrement riche en nutriments, offrant des conditions idéales pour cultiver le café à une altitude entre 500 et 1300 mètres. C'est ainsi que la municipalité de Mosteiros est devenue célèbre pour son café reconnu mondialement : « le café de Fogo ». 

***Le café de marque « Monte Queimado » obtint la Médaille d'Or de l'Empire Portugais à l'Exposition de Porto en 1934!***

Aujourd'hui, à São Filipe, la Maison *Dja’r Fogo* tenue par Agnelo, perpétue la tradition (après six générations) d'un café organique 100% arabica. Toute la production est manuelle, gage de qualité et transparence: entretien des arbustes, collecte, séchage à l'air libre, ablation de l'écorce. La torréfaction est faite sur place et la vente est faite directement aux touristes visitant l'île. 

![São Filipe, l'église (île de Fogo)](/media/uploads/dscf1289.jpg)

Dotée d'une conscience écologique et sociale, *Dja’r Fogo* a eu l'ambitieuse idée de proposer un produit fini grâce à l'acquisition d'un torréfacteur et le packaging artisanal est conçu par Vanusa. Si vous allez à Fogo, je vous conseille d'y faire un tour (c'est aussi un petit musée et lieu de détente).

![Packaging du café de Fogo médaillé d'Or et l'actuel design](/media/uploads/img_7577-2-.jpg)

## Le café, moyen de développement

***Les spécialistes reconnaissent donc que le Cap-Vert possède un café biologique, rare, unique et de qualité supérieure.*** 

Pourtant, le pays a des challenges importants à relever. Deux années de suite de sécheresse ont considérablement réduit le niveau de récolte (2018, 2019). Cette sécheresse empêche tout renouvellement nécessaire des plants de café (certains sont âgés de 200 ans) qui n’est possible qu'en période de pluie. 

Dans le même temps, le pays mise sur l'exportation, et a créé un partenariat «Fogo Coffee Spirit » avec un spécialiste hollandais *Trabocca* qui investit, contrôle la qualité et forme le personnel. *Trabocca*, dont Starbucks est un client, a bien sûr le pouvoir de négociation du prix d’achat. Bon deal pour le petit Cap-Vert? Je ne sais pas, et puis Starbucks c'est plus vraiment ma tasse de thé, mais l'exotisme du produit peut, semble t'il, plaire aux amoureux du café au Japon. 

Quant à moi, je vous dis: 

« venez à Cabo Verde! et profitez-en pour déguster son café ».

Adelino Duarte, 5 septembre 2019.