+++
title = "Ailton Krenak, Verteidiger der Indigenen Völker "
serie = "que-des-numeros-6-dans-ma-team"
slug = "Ailton-Krenak-Verteidiger-Indigenen"
draft = true
language = "de"
themes = ["Inspirations"]
date_published = "2021-03-29"
+++
Sie sind Schützer des Walds und der Umwelt, sie sind die Verteidiger der indigenen Völker. 

## Warum Ailton Krenak?

2016: Ein friedlicher Widerstand gegen den Aufbau einer Pipeline in den USA (Dakota) war ein bedeutendster Fall für das Entstehen meines ökologischen Bewusstsein und für meine Wahrnehmung der Menschenrechtsverletzungen gegen die indigenen Völkern. Die gewaltige Unterdrückung den Demonstranten gegenüber von einer stark militarisierten Polizei im Dienst eines Ölkonzern empörte mich.

2020: Ein US Bundesbeschluss stellt den Aufbau der Dakota Pipeline ein und fordert eine weitere Umweltverträglichkeitsstudie. Es ist hier ein gerichtlicher Sieg obwohl der Kampf nicht vorbei ist. Der neue Präsident Biden hebt die Baugenehmigung der US-Kanadianer Keystone XL Pipeline auf. Diese gute Nachrichten betreffen sowohl die indigene Völker als auch uns Alle.

Die Verteidigung des heiligen Landes: Ich denke, dass sie die echte Schützer unserer Umwelt sind

Im Südamerika und Brasilien ist die aktuelle Situation allerdings sehr besorgniserregend. In diesem Artikel geht es um eine bedeutende Stimme aus Brasilien. Er ist Ailton Krenak, im 1954 geboren, Schützer der Rechte der indigenen Völker, Umweltschützer und Schriftsteller. 2020 wurde er mit dem Preis "Intellektuelles des Jahres" von der brasilianischen Union der Schriftsteller ausgezeichnet.

Ich werde auch seine Genossen und andere Verteidiger, die er oft erwähnt, in meinem nächsten Artikel vorstellen (Chico Mendes, Joenia Wapixana, Marçal de Souza (Tupa-i), Davi Kopenawa, Chef Raoni).

## Die Flucht

Ein tragisches Ereignis, das eine frohe Kindheit im Tal "Rio Doce" und an den gleichnamigen Fluss entlang beendet. Ein Exil in seinem eigenen Land, da Ailton Krenak in seinem eigenen Land ausgeschlossen wurde.

In den siebziger Jahren beherrscht in Brasilien eine gewaltige militärische Diktatur (1964-1985): Sie einschüchtert, ausweist, foltert, tötet die indigene Menschen im Namen der "Reformen und Fortschritt des Landes". Die Aufgabe des Staats bestand darin, der Wald zu erobern. Es bedeutet deshalb die Entwaldung der indigenen Gebiete um große Projekte auszuführen (Straße, Goldminen, Dämme) und große Agrarbetriebe zu entwickeln, die gefräßig nach Oberflächen gezielt für die Rindviehzucht sind.

1973 widersetzten sich Chico Buarque und Milton Nascimento mit dem Lied "Calice" (ein Wörterspiel für "Schweigen" oder "Ruhe!") der Zensur und der Gewalt der Diktatur.

## Die Vereinigung

Im Alter von 17 Jahren lernte er in der Welt der "zivilisierten" zu lesen und schreiben. Er stellte plötzlich den Diskurs und die Vorurteilen von der "Weißen" gegen seine Gemeinschaft fest. Er hört die demütigenden Äußerungen wie "eine zurückgebliebene Gesellschaft", "noch entwickelnde Menschen", "faule unfähige Menschen, die man helfen muss" und "vom Aussterben bedroht" als ob die indigene Menschen ein Tierspezies wären. Ein demütigender Diskurs von der politischen Klasse um die Verneinung der indigenen Rechten zu rechtfertigen.

Das diktatorisches Regime kündigte paradoxerweise die Emanzipation der indigenen Völker an. Denn "die indigene Völker seien auch vollwertige Brasilianer". Aber hinter diesem Diskurs versteckte sich der heimtückische Plan der Diktatur, die indigene Völker durch eine Entziehung ihren Identitäten zu assimilieren und daher sie einfacher verschleppen und ihren Länder rauben können.

Ailton unternahmeine Reise durch das riesiges Land und traf die andere indigene Gruppen (mehr als 300 Gruppen entsprechend 900 000 Menschen wurden heute in Brasilien gerechnet). Er nahm gemeinsam mit anderen Leaders an die Gründung der Union der indigenen Nationen teil.

Die indigene Identität ist grundsätzlich auf die Verbindung den Völker mit der Erde beruht. Es bedeutet eine tiefgreifende Gemeinschaft mit der Natur: Die Erde, der Fluss und das Gebirge sind heilig angesehen denn sie besitzen ein Gedächtnis.

Ihre erste Forderung ist eine Nachfrage nach einer Demarkation ihren angestammten Länder. Es geht um ein historisches und traditionelles Recht. Denn diese Länder gehören tatsächlich zu ihnen und wurden vor der Ankunft der ersten portugiesischen Kolonen von der indigenen Völker gepflegt.

## Ailton und der jenipapo

1987 wurde Ailton Krenak eingeladen, eine Rede vor der verfassunggebenden Versammlung zu halten. Sehr inspiriert trug er den traditionellen jenipapo Lack ("die Frucht, die Flecken verursacht") auf seinen Gesicht auf während er seine Botschaft vermittelt. Seine Rede, die Respekt und Anerkennung verlangte, zwang die 1988 Verfassung ein Kapitel über die "Indianer" einzuschließen. Dieses Kapitel anerkennt die indigene Rechte, Sitten und Gebräuche und ihre Autonomie mit dem bestimmten Artikel 231, der ausdrücklich das historisches Recht auf ihre Gebiete besagt.

## Ailton's Reflexionen und Gedanken

Seine Bücher, Konferenzen und Interviews sind faszinierend. Seine Gedanken lasst uns unbedingt nachdenken.

### Unsere Beziehungen

Im Gegenteil zu der "Weißen" überlegen sich die indigene Völker nicht getrennt von der Natur. Die Überlegung, dass der Mensch auf einer Seite (oder eher an der Spitze der Pyramide) steht und die Natur auf der anderen Seite, ist typischerweise eine europäische Theorie.

In der erlebenden Beziehungen der indigenen Völker werden Sakralisierung, Personalisierung und Verwandtschaft beachtet: Die Erde ist die Mutter, der Fluss hat einen Namen (bei den Krenak Völker, heißt der heilige Fluss "ouatou"), der gegenüberliegende Berg ist der Opa, der Ratschläge gibt. Im solchen respektvollen Ökosystem kann sich die Umwelt mühelos regenerieren.

### Das Treffen zwischen zwei Welten

Vor 500 Jahren, als die zwei Welten sich an der brasilianischen Küsten trafen, stellten sich die Indigene Menschen Fragen nach dem "sein"des Besuchers, nach seinen Gebräuchen und Glauben. Während der Besiedler, wie ein guter Kaufmann, interessierte sich lediglich für den"haben"des Fremdes.

Der Kolonisator hatte eine Obsession: die Natur als ein Gegenstand wahrzunehmen und sie einfach durch unbegrenzte Gewinnung handeln und verkaufen.

Der Kolonisator beschloss nicht nur die Natur auszubeuten sondern auch die Körper. Der indigene Mensch wird einen Gegenstand Sklave, gleichzeitig den Brennstoff und den Motor der intensiven Monokultur in den Plantagen, die nur einigen in Europa bereichern (eine Analogie, die sich unter anderem auf die Arbeit von Malcom Ferdinand bezieht, den ich später erwähne).

### Wer wir sind und die Kraft des Traums

Entfremdet, sind wir nicht in der Lage um "Ich bin lebendig" und "Ich lebe" zu unterscheiden. Ailton zufolge müssen wir den Mut haben, grundlegend lebendig zu sein und "das Überleben nicht zu verhandeln". Das Leben solle nicht nützlich sein sondern ein kosmischer Tanz, der ein Gleichgewicht zwischen der Erde und dem Himmel gestattet. Er begreift nicht wieso man das Leben wie eine einfache absurde nützliche Choreographie reduzieren kann.

Der Traum, transzendent und eine Fähigkeit von Allem, wenn man ihn zu unserer Umgebung erzählt, könnte ein Ereignis in unserem Alltag auslösen. Der Traum könnte auch unsere Entscheidungen lenken. Er ist überzeugt von der Kraft des Imaginäres. Man müsse lernen, von unserer erwünschten Welt zu träumen und dann ihn durch ein Handeln zu verwirklichen.

### Kolonisation und Ökologie

Seit der Kolonisation in Brasilien im Jahr 1500 tauchte der Willen auf, die Existenz der indigenen Völker durch die Schaffung eines absichtliches fehlerhaftes Narrativ zu leugnen, in dem die Indigene ignoriert durch das Entstehen von einem wissenschaftlichen Rassismus wurden.

Mata Atlantica (der atlantische Wald) wurde von den europäischen Naturforscher im 19. Jahrhundert als einer Urwald beschreibt, einer von Gott verlassene wunderschöne Garten Eden. Aber Ailton hebt die Tatsache hervor, dass der Wald bereits mit den Tupi und Guarani Völker bewohnt wurde, die die echte Schöpfer des Walds waren.

Die "Kolonialität" in der Ökologie wird in diesem bestimmten Zusammenhang wahrgenommen, wenn eine Ökologie besteht darin, den traditionellen Einwohner aus seiner natürlichen Umwelt zu entfernen indem er zunächst ignoriert und anschließend deportiert wird.

Im Bezug auf diesem Thema, empfehle ich die Werke vom Umweltschützer und Philosoph Malcom Ferdinand und vom Historiker und Dozent Guillaume Blanc.

Dieses Video "Die Kriege von Brasilien" auf portugiesisch erzählt von der Geschichte der indigenen Völker aus Brasilien:

### Über "das Ende der Welt"

In seinem mit dem provozierenden Titel "Ideen um das Ende der Welt zu verschieben" Buch, fordert uns Ailton Krenak eigentlich darauf, ein kollektives Bewusstsein zu erzeugen damit wir die "Welt von danach" aufbauen. Es ist dafür wichtig zu verlangsamen und "sanft auf die Erde laufen".

Er möchte neu erzählen, wie sich die indigene Völker vor 500 Jahren mit ihren eigenen Ende der Welt auseinandergesetzt hatten. Kolonisation, Viren, Bakterien aus Europa und die Gewalt des Zivilisationsprozess hatten einen Großteil der indigene Bevölkerung vernichtet (es wurde ein Genozid von 12 Millionen Indigenen geschätzt und 90% dieser Bevölkerung wurden nur während des ersten Jahrhundert der Kolonisation zwischen 1500 und 1600 getötet). Natürlich haben sie Widerstand geleistet und sie leisten ihn noch ständig heute.

Ehrlich gesagt, war es nicht ein "Ende" der Welt sondern tatsächlich ein "Krieg" der Welten und es dauert leider weiter mit der Ermordungen von indigenen Menschen und der Feindseligkeit und Missachtung der aktuellen Regierung.

### Über die Humanität

Der "Club der Humanität", ein europäisches Versprechen, ist eigentlich ein beschränkter und exklusiver Club. Im Peripherie, befinden sich die Dritte Welt und die Entwicklungsländer.

Lediglich beruht auf einem Harmonisierungsmodell und unterstützt bei globalen zentralisierte Superstrukturen, entscheidet dieser Club allein die Einschließung von den vermeintlichen Nachzüglern in seine zivilisierte Welt.

Heute wissen wir wohl, dass eine solche beschränkte Perspektive nur eine alleine und fade Monokultur der Ideen pflanzt und verhängt eine einzige Organisationsform geeignet für den Marktbedarf: Eine Ideologie, die alle andere Lebensformen und Arte vom Leben gefährdet.

Die Humanität bedeutet die Welt in all ihrer Vielfalt. Eine Welt mit Pluralität von Ideen und Organisationsformen.

Eine musikalische Pause mit der Sängerin Kaê Guajajara. Sie singt über die Mutter Welt und die Verbindung mit der angestammten Erde.

### Die Ökologie des Desasters

Neben der Covid19 Geißel unter denen die vulnerabel indigene Menschen leiden, auseinandersetzen sich die ländliche Bevölkerung mit anderen Gesundheitsfragen. Es gibt z.B die Vergiftung der Flüssen mit dem Quecksilber benutzt in den Goldminen. Außerdem kann man von dem tragischen Bruch eines Damms beinhaltend toxische Produkte: Die 2015 Mariana Katastrophe in der 272 Menschen ums Leben kamen.

*Darüber hinaus macht die Gier des Agrarbetriebe aus, dass Brasilien der erste Verbraucher von Pestiziden pro Hektar in der Welt ist. Die Gesundheit der Bevölkerung und der Biodiversität ist bedroht.*

Nach Ailton, wenn eine Katastrophe geschieht, wird die bevorzugte Lösung sein, einfach eine Entschädigung zu bezahlen. Die große Firma Vale, die die 2015 Mariana Katastrophe verursachte, hat z.B 5 Milliarden Euro zu dem Minas Gerais Bundesland bezahlt damit keine Gerichtsverfahren stattfinden. Daher eine Ökologie des Desasters ist diejenige die sich in der "Wirtschaft des Desasters" integriert. Die Ökologie des Desasters scheint offensichtlich ein wohl verantwortliches für "Ökozid" und umweltschädliches Wirtschaftsmodell anzunehmen.

### Hoffnungen, Demarcação Já!

Ich möchte diesen Artikel mit einem Hoffnungssignal beenden. Solidaritätsaktionen für die indigene Völker aus Südamerika verstärken sich und ihre Stimmen werden auch stärker. Chef Raoni aus Brasilien fordert eine internationale Verbundenheit um gegen die Entwaldung zu kämpfen. Er betont, dass Amazonas nicht nur eine brasilianische Frage sondern eine weltweite Frage darstellt, die uns Alle betrifft.

Wir könnten z.B unser Respekt für die indigene Völker bekräftigen, ihre Rechte anerkennen und ihre Kämpfe sichtbar machen. Interesse an ihren Kultur zeigen. Wir könnten z.B einen ethischen Konsum bevorzugen und verlangen die Auflösung der unverantwortlichen internationalen Handelsabkommen zwischen Europäische Union und MERCOSUR. Diese sind sicher einige interessante Reflexionen und politischer Handlungsspielraum zu erobern.

Wie Ailton Krenak uns empfiehlt: Lassen wir uns träumen, nützen wir den Traum damit wir ihn in unserem Leben verwirklichen, schaffen wir diese Verbindung damit wir letztendlich zurück in den Armen unser Mutter Erde kehren.