+++
title = "Die Kap \"farblichen\" Inseln"
serie = "cabo-verde"
slug = "die-kap-farblichen-inseln"
language = "de"
themes = ["Travel"]
date_published = "2019-12-07"
image = "/media/uploads/dscf0445.jpg"
image_caption = "Fonti Bila, Fogo Insel"

[[links]]
link = "Das Reisebüro Qualitur auf Fogo Insel [Tripadvisor](https://www.tripadvisor.de/Attraction_Review-g1185367-d8368247-Reviews-Qualitur_Viagens_e_Turismo-Sao_Filipe_Fogo.html)"

[[links]]
link = "Einige Casas in Cha das Caldeiras neben dem Vulkan: [Casa Alcindo](https://www.tripadvisor.de/Hotel_Review-g1185510-d13121357-Reviews-Casa_Alcindo-Cha_das_Caldeiras_Fogo.html) and [Casa Marisa](https://www.tripadvisor.de/Hotel_Review-g1185510-d3964746-Reviews-Casa_Marisa-Cha_das_Caldeiras_Fogo.html)"

[[links]]
link = "Der Schriftsteller Baltasar Lopes Da Silva [Wikipedia](https://de.wikipedia.org/wiki/Baltasar_Lopes_da_Silva)"

[[links]]
link = "Morna, kurzes Video von [UNESCO](https://youtu.be/2e0wCkWeoVs)"

[[links]]
link = "Youtube Doku [Sandgrains](https://youtu.be/ZIQ1iJ4wPvI) über die Umweltkatastrophe auf die Kapverden."

[[links]]
link = "Tripadvisor für Sao Nicolau [Naturalpark Monte Gordo](https://www.tripadvisor.de/Attraction_Review-g482853-d7266969-Reviews-Parque_Natural_do_Monte_Gordo-Sao_Nicolau.html)"

[[links]]
link = "Artikel: Der Legendäre Fußballspieler [EUSEBIO](http://www1.sportschau.de/sportschau_specials/fussball/wm2014/wm_historie/index_49.html)"

[[links]]
link = "Umweltschützer und Schutz der Meeresschildkröten auf Maio Insel: [Maio Biodiversity Foundation](https://www.fmb-maio.org/)"

[[links]]
link = "Meine Inspiration: Wangari Maathai, 2004 Nobelpreisträgerin und der [Green belt movement](https://greenbeltmovement.org/wangari-maathai)"
+++
Die Kapverdischen Inseln (Cabo Verde) weisen ein farbliches Naturspektakel aus. 

Cabo VERDE, Cap-VERT: Auf Portugiesisch und Französisch scheint die Farbe deutlich im Namen des Landes, und zwar "grün". Interessanterweise läßt sich dieses Wortspiel auf Deutsch und auf Englisch nicht verwenden da man Kap "Grün" oder Cape "Green" nicht sagt.

Ich möchte gern über die anwesende Natur und auch über die Wahrnehmung des Thema "Umweltschutz auf diesen kleinen Inseln" reden. Ich stelle mich die Frage: Könnte Cabo Verde von seinem Namen profitieren, indem das Land den Ökotourismus entwickeln kann und ein verantwortliches Reisen anbieten und bewerben kann? 

Ich hoffe, dass meinem Beitrag endlich es gelingt, das Grün des "Petit pays" hervorzuheben.

![Der Ort woher Oma Alexandrina Mariana Fernandes (4Jun1896 - 25Jun1997) kommt: Pinhão, Santo Antão Insel](/media/uploads/dscf0870.jpg)

## Die Geschichte der grüne Farbe

1444 wurde der Begriff "Cabo Verde" zum ersten Mal vom portugiesischen Seefahrer Dinis Dias verwendet als er eine Halbinsel voller üppige Vegetation zu Recht beschreibt. Diese Halbinsel liegt in der Spitze Kontinentalafrikas: der heutige Cap-Vert von Dakar in Senegal. 

Die Kapverdischen Inseln wurden eigentlich im Jahr 1460 nacheinander entdeckt. Die Entdecker hatten sich bloß von Cap-Vert der Afrikaküste inspiriert lassen und deswegen nannten die Inselgruppe auch auf Portugiesisch "Cabo Verde", da heutige Land. 

Die Farbe der Hoffnung kennzeichnet irgendwie eine starke Eigenschaft der Bevölkerung: die Resilienz. Darüber hinaus lässt uns dieser Name "Cabo Verde" offensichtlich an die Natur denken.

![Die Aussicht von Lombo Branco auf Santo Antão Insel. Ein kleiner Teil von Ponto do Sol erscheint im Hintergrund.](/media/uploads/dscf0789.jpg)

## Auf die Suche nach einem Fest der Farben

Eine gelbe herrschende Landschaft eines sonnigen Strand und das türkisblaue Meer. Eine traumhafte Urlaubsvorstellung des Großteil der 700,000 jährlichen Touristen (nach Angaben vom Jahr 2018), die im großen Hotelanlagen auf Sal oder Boa Vista Inseln übernachten.

![Perfekte Erholung auf Boa Vista Insel](/media/uploads/boavista_1-1-.jpg)

Bietet aber Cabo Verde einen farbigeren so genannt Ökotourismus? Ich begleite Sie bei dieser Suche.

![Wanderung von Figueiral, Corda bis zu Cova Krater, auf Santo Antão Insel](/media/uploads/dscf0984.jpg)

## Schwarz, weiß, grau: Lava und Felsen

Das Land ist ein Paradies für die Wanderer und Liebhaber der Natur. Das vulkanische und bergige Umfeld bietet ideale Bedingungen, sodass der Einzelne sich mit der Natur verbindet. 

Unter der sieben Wunder des Lands befindet sich der majestätisch immer noch aktiver Vulkan auf Fogo Insel. Sein Aufstieg ist ein unvergessliches und sportliches Abenteuer. Falls du kein Abenteurer bist, könntest du dich immerhin dafür entscheiden, ein Leben wie die lokale Bevölkerung in Chã das Caldeiras neben den Vulkan zu erleben. 

Dafür gibt es die Casas ([Casa Marisa](https://www.tripadvisor.de/Hotel_Review-g1185510-d3964746-Reviews-Casa_Marisa-Cha_das_Caldeiras_Fogo.html), [Casa Alcindo](https://www.tripadvisor.de/Hotel_Review-g1185510-d13121357-Reviews-Casa_Alcindo-Cha_das_Caldeiras_Fogo.html)) die Sie mit einer erstaunlichen Gastfreundschaft empfangen werden. 

Der Reisespezialist [Qualitur](https://www.tripadvisor.de/Attraction_Review-g1185367-d8368247-Reviews-Qualitur_Viagens_e_Turismo-Sao_Filipe_Fogo.html) könnte dir jederzeit bei deinem Urlaubsplan helfen.

![Pico do Fogo Vulkan auf Fogo Insel](/media/uploads/dscf0467.jpg)

## Blau, Willkommen in Cabo Azul

Das Meer ist ein Bestandteil der Menschen in Cabo Verde. 

Der Autor Baltasar Lopes (1907-1989) beschreibt akkurat dieses besondere Gefühl in seinem Klassiker der Cabo Verde Literatur "Chiquinho" (1947). 

In der Morna Musik geht es oft um das Meer. Das Meer evoziert sowohl Wanderung und Flucht (das Fernweh) als auch *Sodade* (Heimweh) und Traum.

![Die kulturelle Botschafterin Cabo Verde, Cesaria Evora (1941-2011)](/media/uploads/dscf0777.jpg)

Die Erde oder das Meer? Für das Regen beten und folglich die Erde pflügen? Oder an Bord des Schiffes gehen im Versuch sein Leben im Ausland zu verdienen?

## Die Kraft des Atlantik

Es ist Zeit, dass Cabo Verde vernünftig und auf nachhaltige Weise für sein Meer sorgt. 

***Denn die blaue Ökonomie kann wirklich Cabo Verde helfen, grüner zu werden.*** 

Cabo Verde ausschließliche Wirtschaftszone (Meeresgewässer) zählt mehr als 700,000 km². Zum Vergleich: seine insgesamt terrestrische Landfläche entspricht nur 4,033 km². Diese riesige Oberfläche auf dem Nordatlantik gilt als ein großes Potential. Doch es repräsentiert eine noch bedeutendere Verantwortung.

Die Geschichte dieses jungen Staats beweist, dass sich aus dem getrockneten Klima eine beträchtliche Wasserknappheit ergibt, die folglich schlechte Ernte verursacht.

Die sehr kostspielige Vorgänge des Pumpen der Meerwasserentsalzungsanlagen lassen uns die Frage nach der Energieeffizienz stellen. Der Strompreis auf Cabo Verde ist dreimal höher als in Europa. Vor diesem Hintergrund stellen die erneuerbare Energien eine gegenwärtige nachhaltige Lösung dar. 

Ein endgültiger Zugang zu günstigerem sauberem Wasser durch eine mit versorgter Solarenergie oder Windkraft Meerwasserentsalzung wäre eine große Errungenschaft für das Land.

"Le petit pays" hat eine riesige Verantwortung im Feld des Schutz der Biodiversität. Unverantwortliche menschliche Aktivitäten im Ozean gefährden unmittelbar die lokale Fischergemeinden und die Tieren. 

Langjährige intensive Fischerei, Überfischung ausgeübt von Hochseetrawlers aus Europa, Japan und China und der Haifischfang beschädigen unseres Meeresökosystem (die wertvolle Koralle, Seevögel, Haien...) und die einheimische Wirtschaft. 

Was die handwerkliche Fischerei angeht, trägt sie normalerweise zu Ernährungssicherheit und Schaffung von Arbeitsplätzen bei. Doch ist diese Aktivität in Schwierigkeiten geraten und die Anzahl von Fischer sinkt erheblich.

![Biodiversität auf Fonti Bila Strand auf Fogo Insel.](/media/uploads/dscf0681.jpg)

## Kap Grün

Wußten Sie, dass die Natur beziehungsweise die grüne Oberfläche auf Cabo Verde sich aus menschlichen Einfallsreichtum ergibt? 

Die Bevölkerung bildet das Land angesichts von Widrigkeiten mit bloßen Händen. Da die Inseln zur Sahelzone gehören, stellt die geographische Lage eine Vulnerabilität dar: Trockenheit, Wasserknappheit und Erosion. Dies hat eine große Auswirkung auf den Anbau. 

Während der Kolonialzeit wurden mehrere Tausende Menschen an schlechten Zeiten an Hungersnoten gestorben. Nach der 1975 Unabhängigkeit began eine alltägliche Bekämpfung gegen der Trockenheit und der Erosion von Böden durch ein Aufforstungsprojekt. Innerhalb 40 Jahre wurde die Oberflächen von 5% auf 21% aufgeforstet nämlich 840 km², indem verschiedene Obstbäume gepflanzt wurden. Der Staat zufolge werden noch 8 Millionen Bäume anhand der Vereinigten Nationen nachhaltige Entwicklung 2030 Agenda weitergepflanzt. 

Heutzutage besitzt Cabo Verde zweifellos eine Gewandtheit im Umgang mit dem Anbau in Extrembedingungen.

![Figueiral auf Santo Antão Insel](/media/uploads/img_7470.jpg)

In Bezug auf Ökotourismus besitzt Cabo Verde wunderbare grüne Landschaften beispielsweise atemberaubende Täler auf Santo Antão und Santiago Inseln, Naturparks auf Fogo und ***[São Nicolau](https://www.tripadvisor.de/Attraction_Review-g482853-d7266969-Reviews-Parque_Natural_do_Monte_Gordo-Sao_Nicolau.html)*** Inseln.

## Rot, Benfica und Fußball

Wenn ich die rote Farbe mit Cabo Verde verbinden soll, denke ich an die Hibiskus Blume. Doch gibt es viele Sorte von dieser Blume und nicht nur die Rot.   

![Rote Hibiskus Blume auf Brava Insel](/media/uploads/dscf1451.jpg)

Anschließend errinere ich mich an die populärste Fußballmannschaft in Cabo Verde: Die aus Portugal Benfica Lissabon, deren rotes Trikot sehr häufig auf der Inseln getragen ist.

Drei großartige Spieler nahmen an die Entstehung der Benfica Begeisterung auf Cabo Verde teil. Erstens, soll ich nennen die Portugiesisch aus Mozambik "schwarze Panther" Legende Eusebio (1942-2014). 

Zwei andere alte Stolz der Inseln, die bei Benfica in der 70ern gespielt haben, sind Carlos Alhinho (1949-2008) und Alberto Gomes Fonseca Junior (Enkel von Kapverdiern, geboren 1956 in Guinea-Bissau). Seitdem ist die Fußballbegeisterung noch intakt. Die 1920 gegründete Fußballmannschaft "Mindelese" auf São Vicente Insel ist auch ziemlich populär. Errate die Farbe ihres Trikots!

![Flyer der Eusebio Ausstellung in Lissabon](/media/uploads/dscf2749-2-.jpg)

## Ein Wohlergehen anhand des ökologische grün

Zunächst möchte ich darauf hinweisen, dass drei viertel der globalen kumulierten Kohlendioxid und Treibhausgas Emissionen aus den nördlichen Länder stammen.

Der ganze Afrika Kontinent ist nur für 4% des CO2 Ausstoß verantwortlich, dennoch betrifft alle Länder die Umweltfrage. 

Die auf globaler Ebene Ozeanübersäuerung, der Verlust des Biodiversitäts und die Erhöhung des Wasserspiegels sind tatsächlich schwer beunruhigend. 

Es ist deutlich, dass in unserer aktuellen Anthropozän Epoche, werden auch in Cabo Verde unverantwortliche umweltschädliche Verhalten und deutlich mangelnde Umweltpolitik betrachtet (z.B: Sozialen Fragen, Sandgewinnung und folgende Erosion). Plastikverschmutzung, Abfallwirtschaft und Verschmutzung des Meer: Die Inseln einführen 90% ihrer Gütern:

![](/media/uploads/dscf1323.jpg)

Davon abgesehen, können wir verschiedene positive Handeln im Feld des Umweltschutz in Cabo Verde anerkennen. 

Erstens, wie bereits in diesem Beitrag erwähnt, gehört das geniale menschliche Eingreifen in landwirtschaftliche Nutzfläche (Bewaldung) zu einem großartigen agronomischen Können. 

Zweitens, sind die Umweltschützer auf Cabo Verde sehr akitv und führen bemerkenswerte Projekte: 

Z.B ein Meeresschildkröten Schutzprojekt wie ***Maio Biodiversity Foundation***. 

Drittens, gibt es ein politischer Wille für eine Durchdringung der erneuerbare Energie von 50% des Energiemix bis 2030. Die Energiewende ist dringend und unverzichtbar um die hohe Abhängigkeit von fossilen Brennstoffen aufzuhalten.

Zum Schluss, scheint die Energiewende als eine interessante Chance für "Kleine Inselentwicklungslands" (Small Island Developing State) wie Cabo Verde um ein Wohlergehen der Bevölkerung zu erreichen und gleichseitig umweltfreundlicher zu werden. Nur werden die maßgeblichen Investitionen gebraucht...

## Meine aktuelle Inspiration

Mein Name ist Adelino Duarte und ich wohne jetzt in Berlin. Als "fidjo di Kriolo" bin ich fasziniert von dem Land meinen Eltern, Cabo Verde. 

Dank der 2004 aus Kenia Nobelpreisträgerin ***Wangari Maathai*** (1940-2011) wurde mein Umweltbewusstsein verstärkt, nachdem ich ihr Buch "The Challenge for Africa" gelesen hatte. 

Ich bin überzeugt davon, dass die Umwelt eine Schlüsselrolle bei allen politischen, gesellschaftlichen und wirtschaftlichen Entscheidungen spielt. 

Ich versuche hoffnungsvoll ein Umweltbewusstsein zu wecken und die Schönheit von Afrika durch meine Imaginäre zu offenbaren.

Dieser Beitrag ist für meine Lieblingsinsel Santo Antão und ihre Bevölkerung, os Santantonese.

Adelino Duarte, am 7. Dezember 2019